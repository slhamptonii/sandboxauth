module gitlab.com/slhamptonii/sandboxauth

go 1.14

require (
	github.com/aws/aws-lambda-go v1.17.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/stretchr/testify v1.5.1
	gitlab.com/slhamptonii/sheldonsandbox-core/v2 v2.2.0
)
