package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"log"
	"os"
	"regexp"
	"time"
)

type Permissions struct {
	Permits map[string]map[string][]*regexp.Regexp
}

type Claims struct {
	Role model.Role `json:"role"`
	jwt.StandardClaims
}

var key = []byte(os.Getenv("SANDBOX_AUTH_KEY"))
var sauce = os.Getenv("SANDBOX_SAUCE")
var permissions Permissions
var roleResourceMap map[model.Role][]string
var apiGateway string

func init() {
	apiGateway = os.Getenv("SANDBOX_API_GATEWAY")
	permissions = Permissions{
		Permits: map[string]map[string][]*regexp.Regexp{
			model.Stranger.String(): {
				"POST": []*regexp.Regexp{
					regexp.MustCompile(`^/login$`),
					regexp.MustCompile(`^/users$`),
				},
			},
			model.Player.String(): {
				"POST": []*regexp.Regexp{
					regexp.MustCompile(`^/users/[a-zA-Z_-]{4,15}/items$`),
					regexp.MustCompile(`^/parties$`),
				},
				"GET": []*regexp.Regexp{
					regexp.MustCompile(`^/users/[a-zA-Z_-]{4,15}/items$`),
					regexp.MustCompile(`^/users/[a-zA-Z_-]{4,15}/items/[a-z0-9-]{36}$`),
					regexp.MustCompile(`^/parties/[a-zA-Z_-]{4,15}$`),
				},
				"DELETE": []*regexp.Regexp{
					regexp.MustCompile(`^/users/[a-zA-Z_-]{4,15}$`),
					regexp.MustCompile(`^/users/[a-zA-Z_-]{4,15}/items$`),
					regexp.MustCompile(`^/users/[a-zA-Z_-]{4,15}/items/[a-z0-9-]{36}$`),
					regexp.MustCompile(`^/parties/[a-zA-Z_-]{4,15}$`),
				},
			},
		},
	}
	roleResourceMap = map[model.Role][]string{
		model.Stranger: {
			apiGateway + "/*/POST/login",
			apiGateway + "/*/POST/users",
		},
		model.Player: {
			apiGateway + "/*/DELETE/users/*",
			apiGateway + "/*/POST/users/*/items",
			apiGateway + "/*/GET/users/*/items",
			apiGateway + "/*/GET/users/*/items/*",
			apiGateway + "/*/DELETE/users/*/items",
			apiGateway + "/*/DELETE/users/*/items/*",
		},
	}
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayCustomAuthorizerResponse, error) {
	log.Println(r.Headers)
	signed := r.Headers["x-ss"]
	var claims Claims

	if checkSignedString(signed) {
		claims.Role = model.Stranger
		authRes := authorize(r.HTTPMethod, r.Path, claims)

		if authRes.PolicyDocument.Statement[0].Effect == "Allow" {
			log.Println("making token")
			ss, err := newToken(model.Player)
			if err != nil {
				log.Println("could not generate new token", err)
				return generatePolicy("*", "*", "Deny", []string{"*"}), nil
			}

			authRes.Context["ss"] = ss
		}

		return authRes, nil
	} else {
		token, err := jwt.ParseWithClaims(signed, &claims, func(token *jwt.Token) (interface{}, error) {
			return key, nil
		})

		if token == nil || !token.Valid {
			if ve, ok := err.(*jwt.ValidationError); ok {
				if ve.Errors&jwt.ValidationErrorMalformed != 0 {
					log.Println("token was malformed", ve)
				} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
					log.Println("token expired or not yet active", ve)
				} else {
					log.Println("token was invalid", ve)
				}
			}
			return generatePolicy("*", "*", "Deny", []string{"*"}), nil
		}

		authRes := authorize(r.HTTPMethod, r.Path, claims)

		if authRes.PolicyDocument.Statement[0].Effect == "Allow" {
			authRes.Context["ss"] = signed
		}

		return authRes, nil
	}
}

func checkSignedString(signed string) bool {
	log.Println("sauce", sauce, "signed", signed)
	return signed == sauce
}

func authorize(method, path string, claims Claims) events.APIGatewayCustomAuthorizerResponse {
	log.Println("evaluating role")
	if !evaluateRole(method, path, claims) {
		return generatePolicy("*", "*", "Deny", []string{"*"})
	}

	return generatePolicy("apigateway.amazonaws.com", "execute-api:Invoke", "Allow", roleResourceMap[claims.Role])
}

func evaluateRole(method, path string, claims Claims) bool {
	if role, ok := permissions.Permits[claims.Role.String()]; ok {
		if val, ok := role[method]; ok {
			for _, reg := range val {
				if reg.MatchString(path) {
					return true
				}
			}
		}
	}

	return false
}

func generatePolicy(principalID, action, effect string, resources []string) events.APIGatewayCustomAuthorizerResponse {
	return events.APIGatewayCustomAuthorizerResponse{
		PrincipalID: principalID,
		PolicyDocument: events.APIGatewayCustomAuthorizerPolicy{
			Version: "2012-10-17",
			Statement: []events.IAMPolicyStatement{
				{
					Action:   []string{action},
					Effect:   effect,
					Resource: resources,
				},
			},
		},
		Context: map[string]interface{}{},
	}
}

func newToken(role model.Role) (string, error) {
	claims := Claims{
		role,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 2).Unix(),
			Issuer:    "sandboxauth",
			IssuedAt:  time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(key)
}

func main() {
	lambda.Start(HandleRequest)
}
