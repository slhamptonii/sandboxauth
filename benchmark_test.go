package main

import (
	"bytes"
	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"log"
	"math/rand"
	"testing"
)

func benchmarkHandleRequest(b *testing.B) {
	//swallow the logs
	var str bytes.Buffer
	log.SetOutput(&str)

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var ss string
			opts := rand.Intn(2)
			req := events.APIGatewayProxyRequest{
				Path:       randomPath(),
				HTTPMethod: randomMethod(),
				Headers:    map[string]string{},
			}

			switch opts {
			case 0:
				ss = randomString(32)
			case 1:
				tmp := randomString(32)
				ss = tmp
				sauce = tmp
			case 2:
				token, err := newToken(randomRole())
				if err != nil {
					b.Errorf("unable to create Token: %v", err)
				}
				ss = token
			}

			req.Headers["X-ss"] = ss

			_, _ = HandleRequest(req)
		}
	})
}

func benchmarkCheckSignedString(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var str string

			if rand.Intn(1) == 0 {
				str = randomString(32)
				sauce = randomString(32)
			} else {
				tmp := randomString(32)
				str = tmp
				sauce = tmp
			}
			_ = checkSignedString(str)
		}
	})
}

func benchmarkAuthorize(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_ = authorize(randomMethod(), randomPath(), Claims{Role: randomRole()})
		}
	})
}

func benchmarkEvaluateRole(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_ = evaluateRole(randomMethod(), randomPath(), Claims{Role: randomRole()})
		}
	})
}

func benchmarkGeneratePolicy(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_ = generatePolicy("*", "*", "Deny", []string{"*"})
		}
	})
}

func benchmarkNewToken(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_, err := newToken(randomRole())
			if err != nil {
				b.Errorf("unable to create Token: %v", err)
			}
		}
	})
}

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func stringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	setLen := len(charset)
	for i := range b {
		b[i] = charset[rand.Intn(setLen)]
	}
	return string(b)
}

func randomString(n int) string {
	return stringWithCharset(rand.Intn(n), charset)
}

func randomPath() string {
	return randomStringFromSlice([]string{"/login", "/users", "/users/test", "/users/test/items", "/users/test/items/dirk"})
}

func randomMethod() string {
	return randomStringFromSlice([]string{"OPTIONS", "HEAD", "POST", "GET", "PUT", "PATCH", "DELETE"})
}

func randomStringFromSlice(arr []string) string {
	return arr[rand.Intn(len(arr)-1)]
}

func randomRole() model.Role {
	return model.Role(rand.Intn(3))
}

func BenchmarkHandleRequest(b *testing.B)     { benchmarkHandleRequest(b) }
func BenchmarkCheckSignedString(b *testing.B) { benchmarkCheckSignedString(b) }
func BenchmarkAuthorize(b *testing.B)         { benchmarkAuthorize(b) }
func BenchmarkEvaluateRole(b *testing.B)      { benchmarkEvaluateRole(b) }
func BenchmarkGeneratePolicy(b *testing.B)    { benchmarkGeneratePolicy(b) }
func BenchmarkNewToken(b *testing.B)          { benchmarkNewToken(b) }
