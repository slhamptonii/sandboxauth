package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"strings"
	"testing"
)

func TestHandleRequestNoTokenHappyPath(t *testing.T) {
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		Path:       "/users",
		HTTPMethod: "POST",
		Headers: map[string]string{
			"x-ss": "kiwi",
		},
	})
	if err != nil {
		t.Error("could not process request", err)
		return
	}

	assert.Equal(t, "apigateway.amazonaws.com", res.PrincipalID)
	assert.NotEqual(t, "", res.Context["ss"])
	assert.Equal(t, "Allow", res.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{"execute-api:Invoke"}, res.PolicyDocument.Statement[0].Action)
	assert.Equal(t, "Allow", res.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{
		"sandbox-gateway/*/POST/login",
		"sandbox-gateway/*/POST/users",
	}, res.PolicyDocument.Statement[0].Resource)
}

func TestHandleRequestTokenHappyPath(t *testing.T) {
	token, err := newToken(model.Player)
	if err != nil {
		t.Error("could not generate test token", err)
	}

	res, err := HandleRequest(events.APIGatewayProxyRequest{
		Path:       "/users/phil/items",
		HTTPMethod: "POST",
		Headers: map[string]string{
			"x-ss": token,
		},
	})
	if err != nil {
		t.Error("could not process request", err)
		return
	}

	assert.Equal(t, "apigateway.amazonaws.com", res.PrincipalID)
	assert.NotEqual(t, "", res.Context["ss"])
	assert.Equal(t, "Allow", res.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{"execute-api:Invoke"}, res.PolicyDocument.Statement[0].Action)
	assert.Equal(t, "Allow", res.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{
		"sandbox-gateway/*/DELETE/users/*",
		"sandbox-gateway/*/POST/users/*/items",
		"sandbox-gateway/*/GET/users/*/items",
		"sandbox-gateway/*/GET/users/*/items/*",
		"sandbox-gateway/*/DELETE/users/*/items",
		"sandbox-gateway/*/DELETE/users/*/items/*",
	}, res.PolicyDocument.Statement[0].Resource)
}

func TestHandleRequestTokenUnauthorized(t *testing.T) {
	token, err := newToken(model.Stranger)
	if err != nil {
		t.Error("could not generate test token", err)
	}

	res, err := HandleRequest(events.APIGatewayProxyRequest{
		Path:       "/users/phil/items",
		HTTPMethod: "POST",
		Headers: map[string]string{
			"x-ss": token,
		},
	})
	if err != nil {
		t.Error("could not process request", err)
		return
	}

	assert.Equal(t, "*", res.PrincipalID)
	assert.Equal(t, []string{"*"}, res.PolicyDocument.Statement[0].Action)
	assert.Equal(t, "Deny", res.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{"*"}, res.PolicyDocument.Statement[0].Resource)
	assert.NotNil(t, res.Context)
}

func TestHandleRequestInvalidTokenMalformed(t *testing.T) {
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		Path:       "/users/phil/items",
		HTTPMethod: "POST",
		Headers: map[string]string{
			"x-ss": "pear",
		},
	})

	assert.Nil(t, err)
	assert.NotNil(t, res.Context)
	assert.Equal(t, "*", res.PrincipalID)
	assert.Equal(t, []string{"*"}, res.PolicyDocument.Statement[0].Action)
	assert.Equal(t, "Deny", res.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{"*"}, res.PolicyDocument.Statement[0].Resource)
}

func TestCheckSignedString(t *testing.T) {
	assert.Equal(t, true, checkSignedString("kiwi"))
	assert.Equal(t, false, checkSignedString("grapefruit"))
	assert.Equal(t, false, checkSignedString("pear"))
}

func TestAuthorize(t *testing.T) {
	//Deny
	authRes := authorize("HEAD", "/", Claims{Role: model.Stranger})
	assert.Equal(t, "*", authRes.PrincipalID)
	assert.Equal(t, []string{"*"}, authRes.PolicyDocument.Statement[0].Action)
	assert.Equal(t, "Deny", authRes.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{"*"}, authRes.PolicyDocument.Statement[0].Resource)
	assert.NotNil(t, authRes.Context)

	//Allow model.Stranger
	authRes = authorize("POST", "/login", Claims{Role: model.Stranger})
	assert.Equal(t, "apigateway.amazonaws.com", authRes.PrincipalID)
	assert.Equal(t, "2012-10-17", authRes.PolicyDocument.Version)
	assert.Equal(t, []string{"execute-api:Invoke"}, authRes.PolicyDocument.Statement[0].Action)
	assert.Equal(t, "Allow", authRes.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{
		"sandbox-gateway/*/POST/login",
		"sandbox-gateway/*/POST/users",
	}, authRes.PolicyDocument.Statement[0].Resource)
	assert.NotNil(t, authRes.Context)

	//Allow model.Player
	authRes = authorize("POST", "/users/cecil/items", Claims{Role: model.Player})
	assert.Equal(t, "apigateway.amazonaws.com", authRes.PrincipalID)
	assert.Equal(t, "2012-10-17", authRes.PolicyDocument.Version)
	assert.Equal(t, []string{"execute-api:Invoke"}, authRes.PolicyDocument.Statement[0].Action)
	assert.Equal(t, "Allow", authRes.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{
		"sandbox-gateway/*/DELETE/users/*",
		"sandbox-gateway/*/POST/users/*/items",
		"sandbox-gateway/*/GET/users/*/items",
		"sandbox-gateway/*/GET/users/*/items/*",
		"sandbox-gateway/*/DELETE/users/*/items",
		"sandbox-gateway/*/DELETE/users/*/items/*",
	}, authRes.PolicyDocument.Statement[0].Resource)
	assert.NotNil(t, authRes.Context)
}

func TestEvaluate(t *testing.T) {
	//model.Stranger
	assert.Equal(t, true, evaluateRole("POST", "/login", Claims{Role: model.Stranger}))
	assert.Equal(t, true, evaluateRole("POST", "/users", Claims{Role: model.Stranger}))
	assert.Equal(t, false, evaluateRole("POST", "/users/carl/items", Claims{Role: model.Stranger}))
	assert.Equal(t, false, evaluateRole("GET", "/users/deborah/items", Claims{Role: model.Stranger}))
	assert.Equal(t, false, evaluateRole("GET", "/users/peter/items/charlie", Claims{Role: model.Stranger}))
	assert.Equal(t, false, evaluateRole("DELETE", "/users/wonda", Claims{Role: model.Stranger}))
	assert.Equal(t, true, evaluateRole("DELETE", "/users/anna/items", Claims{Role: model.Player}))
	assert.Equal(t, false, evaluateRole("DELETE", "/users/lynn/items/shield", Claims{Role: model.Stranger}))

	//model.Player
	assert.Equal(t, false, evaluateRole("POST", "/login", Claims{Role: model.Player}))
	assert.Equal(t, false, evaluateRole("POST", "/users", Claims{Role: model.Player}))
	assert.Equal(t, true, evaluateRole("POST", "/users/carl/items", Claims{Role: model.Player}))
	assert.Equal(t, true, evaluateRole("GET", "/users/deborah/items", Claims{Role: model.Player}))
	assert.Equal(t, true, evaluateRole("GET", "/users/peter/items/34684b96-4814-4730-b98a-22b551e23658", Claims{Role: model.Player}))
	assert.Equal(t, true, evaluateRole("DELETE", "/users/wonda", Claims{Role: model.Player}))
	assert.Equal(t, true, evaluateRole("DELETE", "/users/anna/items", Claims{Role: model.Player}))
	assert.Equal(t, true, evaluateRole("DELETE", "/users/lynn/items/34224b96-4814-4730-b98a-22b533e23658", Claims{Role: model.Player}))

	//invalid username
	assert.Equal(t, false, evaluateRole("GET", "/users/xin/items", Claims{Role: model.Player}))

	//invalid item name
	assert.Equal(t, false, evaluateRole("GET", "/users/isaiah/items/tie", Claims{Role: model.Player}))
}

func TestGeneratePolicy(t *testing.T) {
	policy := generatePolicy("pID", "action", "effect", []string{"resource"})
	assert.Equal(t, "pID", policy.PrincipalID)
	assert.Equal(t, "2012-10-17", policy.PolicyDocument.Version)
	assert.Equal(t, []string{"action"}, policy.PolicyDocument.Statement[0].Action)
	assert.Equal(t, "effect", policy.PolicyDocument.Statement[0].Effect)
	assert.Equal(t, []string{"resource"}, policy.PolicyDocument.Statement[0].Resource)
	assert.NotNil(t, policy.Context)
}

func TestNewToken(t *testing.T) {
	ss, err := newToken(model.Stranger)
	if err != nil {
		t.Error("could not generate new token", err)
		return
	}
	fields := strings.Split(ss, ".")
	assert.Equal(t, 3, len(fields))
	for _, field := range fields {
		assert.NotEqual(t, "", field)
	}
}
